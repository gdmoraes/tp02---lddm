package com.example.gustavodutra.lddm2;

public class Content {

	private String texto;

	public Content(){
		texto = "";
	}

	public void setTexto(String texto){
		this.texto = texto;
	}

	public String getTexto(){
		return texto;
	}
}
