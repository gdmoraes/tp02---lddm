package com.example.gustavodutra.lddm2;

import java.util.ArrayList;

public class Node {

	private Object conteudo;
	private String nome;
	private boolean folha;

	public Node(boolean isLeaf){
		if (isLeaf == false){
			conteudo = new ArrayList<Node>();
			folha = false;
		}else{
			conteudo = new Content();
			folha = true;
		}
	}

	public Object getConteudo() {
		return conteudo;
	}

	public void setConteudo(Object conteudo) {
		this.conteudo = conteudo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isFolha() {
		return folha;
	}

	public void setFolha(boolean folha) {
		this.folha = folha;
	}
}
